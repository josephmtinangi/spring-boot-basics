package io.github.josephmtinangi.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTests {

	@Test
	public void can_create_user() {

		String username = "username";

		User user = new User();
		user.setUsername(username);

		assertEquals(user.getUsername(), username);
	}
}
