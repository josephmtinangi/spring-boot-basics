package io.github.josephmtinangi.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/")
public class HomeController {

	@RequestMapping(path = "", method = RequestMethod.GET)
	public String index() {

		return "Home";
	}
}
